#!/usr/bin/env python
import numpy

import chainer
import chainer.functions as F
import chainer.links as L


class Generator(chainer.Chain):

    def __init__(self, n_hidden=100):
        super(Generator, self).__init__()
        self.n_hidden = n_hidden

        with self.init_scope():
            self.l1 = L.Linear(None, 256)
            self.l2 = L.Linear(None, 512)
            self.out = L.Linear(None, 28*28)

    def make_hidden(self, batchsize):
        dtype = chainer.get_dtype()
        return numpy.random.uniform(-1, 1, (batchsize, self.n_hidden, 1, 1))\
            .astype(dtype)

    def forward(self, z):
        h = F.relu(self.l1(z))
        h = F.relu(self.l2(h))
        h = F.sigmoid(self.out(h))
        x = F.reshape(h, (-1, 1, 28, 28))
        return x


class Discriminator(chainer.Chain):

    def __init__(self):
        super(Discriminator, self).__init__()
        with self.init_scope():
            self.l1 = L.Linear(None, 512)
            self.l2 = L.Linear(None, 256)
            self.out = L.Linear(None, 1)

    def forward(self, x):
        h = F.relu(self.l1(x))
        h = F.relu(self.l2(h))
        h = self.out(h)
        return h
